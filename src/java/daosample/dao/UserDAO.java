/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package daosample.dao;

import daosample.entity.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ApplicationScoped;
import javax.sql.DataSource;

/**
 *
 * @author ivgratchev
 */
@ManagedBean
@ApplicationScoped
public class UserDAO implements IUserDAO {

    @Resource(name = "java:comp/env/jdbc/users")
    private DataSource ds;

    /**
     * Creates a new instance of UserDAO
     */
    public UserDAO() {
    }

    @PostConstruct
    private void postConstruct() {
        System.out.println("UserDAO instantiated");
        System.out.println("data source is " + ds);
    }
    
    public void setDataSource(DataSource ds) {
        this.ds = ds;
    }

    @Override
    public List<User> findAll() {
        System.out.println("UserDAO.findAll() called");
        List<User> result = new ArrayList<User>();
        Connection conn = null;
        try {
            conn = ds.getConnection();
            PreparedStatement stmt = conn.prepareStatement(
                    "SELECT `users`.`id`, `users`.`name`, `users`.`login`, `users`.`pass` FROM `users`");
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                User user = new User();
                user.setId(rs.getInt(1));
                user.setName(rs.getString(2));
                user.setLogin(rs.getString(3));
                user.setPassword(rs.getString(4));

                result.add(user);
            }
            stmt.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    // можно игнорировать исключение при закрытии соединения
                }
            }
        }
        return result;
    }
}
