/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package daosample.mbean;

import daosample.dao.IUserDAO;
import daosample.entity.User;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author ivgratchev
 */
@ManagedBean
@RequestScoped
public class IndexPageBean {

    @ManagedProperty("#{userDAO}")
    private IUserDAO userDAO;
    private List<User> users;

    /**
     * Creates a new instance of IndexPageBean
     */
    public IndexPageBean() {
    }

    public List<User> getUsers() {
        System.out.println("IndexPageBean.getUsers() called");
        if (users == null) {
            users = userDAO.findAll();
        }
        return users;
    }

    public void setUserDAO(IUserDAO userDAO) {
        this.userDAO = userDAO;
    }
}
