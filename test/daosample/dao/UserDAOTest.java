/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package daosample.dao;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import daosample.entity.User;
import java.util.List;
import javax.sql.DataSource;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ivgratchev
 */
public class UserDAOTest {
    private static DataSource ds;
    
    public UserDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        MysqlDataSource mysqlDS = new MysqlDataSource();
        mysqlDS.setUrl("jdbc:mysql://localhost/cinema");
        mysqlDS.setUser("root");
        mysqlDS.setPassword("1234");
        ds = mysqlDS;
    }

    /**
     * Test of findAll method, of class UserDAO.
     */
    @Test
    public void testFindAll() {
        System.out.println("findAll");
        UserDAO instance = new UserDAO();
        instance.setDataSource(ds);
        List<User> result = instance.findAll();
        System.out.println(result);
        assertTrue(result.size() > 0);
    }
}
